/********************Bài 1**********************/
function getId(id) {
  return document.querySelector(id);
}
function btnTable() {
  var result = "";
  var count = 0;
  var a = 1;
  while (a <= 100) {
    result += a + " ";
    if (a++) {
      count++;
    }
    if (count == 10) {
      result += "<br>";
      count = 0;
    }
  }
  getId("#thongBao1").innerHTML = /*html*/ `
    <p>${result}</p>
    `;
}

/********************Bài 2**********************/
// hàm đẩy số nguyên vào mảng
var arrSoNguyen = [];
function btnThemSo() {
  var soNguyen = getId("#txt-bai2").value * 1;
  arrSoNguyen.push(soNguyen);
  getId("#thongBao2").innerHTML = arrSoNguyen;
}

function getSNT(arr) {
  var flag = true;
  if (arr < 2) {
    return (flag = false);
  }
  for (var iSo = 2; iSo < arr; iSo++) {
    if (arr % iSo == 0) {
      flag = false;
      break;
    } else {
      flag = true;
    }
  }
  return flag;
}
function btnTimSNT() {
  var result = [];
  for (var index = 0; index < arrSoNguyen.length; index++) {
    var checkSNT = getSNT(arrSoNguyen[index]);
    if (checkSNT == true) {
      result.push(arrSoNguyen[index]);
    }
  }
  getId("#thongBaoSNT").innerHTML = result.sort(function (a, b) {
    return a - b;
  });
}

/********************Bài 3**********************/
function congThuc(n) {
  var tong = 0;
  for (var giaTri = 2; giaTri <= n; giaTri++) {
    tong += giaTri;
  }
  return tong;
}
function tinhTong() {
  var number = getId("#txt-number3").value * 1;
  if (number == 1) {
    return;
  }
  var m = "2";
  var veSau = Number(m + number);
  var ketQua = 0;
  var a = congThuc(number);
  ketQua = a + veSau;
  getId("#thongBao3").innerHTML = ketQua;
}

/********************Bài 4**********************/
function timUoc() {
  var soLuongUoc = [];
  var input4 = getId("#txt-number4").value * 1;
  for (var n = 0; n <= input4; n++) {
    if (input4 % n === 0) {
      soLuongUoc.push(n);
    }
  }
  getId("#thongBao4").innerHTML = soLuongUoc.sort(function (a, b) {
    return b - a;
  });
}

/********************Bài 5**********************/
function daoSo(n) {
  n = n.toString();
  return n.split("").reverse().join("");
}
function daoNguoc() {
  var number5 = getId("#txt-number5").value * 1;
  getId("#thongBao5").innerHTML = daoSo(number5);
}

/********************Bài 6**********************/
function timSNDLN() {
  var sum = 0;
  for (var x = 1; x <= 100; x++) {
    sum += x;
    if (sum < 100) {
      var soLonNhat = x;
    }
  }
  getId("#thongBao6").innerHTML = soLonNhat;
}

/********************Bài 7**********************/
var bangCuuChuong = [];
function inBangCuuChuong() {
  var ketQua = 0;
  var input7 = getId("#txt-number7").value * 1;
  for (var iSo = 0; iSo <= 10; iSo++) {
    if (iSo == 0) {
      ketQua = input7 * iSo;
      bangCuuChuong.push(`<p>${input7} * ${iSo} = ${ketQua}</p>`);
    } else if (iSo == 1) {
      ketQua = input7 * iSo;
      bangCuuChuong.push(`<p>${input7} * ${iSo} = ${ketQua}</p>`);
    } else if (iSo == 2) {
      ketQua = input7 * iSo;
      bangCuuChuong.push(`<p>${input7} * ${iSo} = ${ketQua}</p>`);
    } else if (iSo == 3) {
      ketQua = input7 * iSo;
      bangCuuChuong.push(`<p>${input7} * ${iSo} = ${ketQua}</p>`);
    } else if (iSo == 4) {
      ketQua = input7 * iSo;
      bangCuuChuong.push(`<p>${input7} * ${iSo} = ${ketQua}</p>`);
    } else if (iSo == 5) {
      ketQua = input7 * iSo;
      bangCuuChuong.push(`<p>${input7} * ${iSo} = ${ketQua}</p>`);
    } else if (iSo == 6) {
      ketQua = input7 * iSo;
      bangCuuChuong.push(`<p>${input7} * ${iSo} = ${ketQua}</p>`);
    } else if (iSo == 7) {
      ketQua = input7 * iSo;
      bangCuuChuong.push(`<p>${input7} * ${iSo} = ${ketQua}</p>`);
    } else if (iSo == 8) {
      ketQua = input7 * iSo;
      bangCuuChuong.push(`<p>${input7} * ${iSo} = ${ketQua}</p>`);
    } else if (iSo == 9) {
      ketQua = input7 * iSo;
      bangCuuChuong.push(`<p>${input7} * ${iSo} = ${ketQua}</p>`);
    } else if (iSo == 10) {
      ketQua = input7 * iSo;
      bangCuuChuong.push(`<p> ${input7} * ${iSo} = ${ketQua} </p>`);
    }
  }
  getId("#thongBao7").innerHTML = bangCuuChuong;
}

/********************Bài 8**********************/
var cards = [
  "4K",
  "KH",
  "5C",
  "KA",
  "QH",
  "KD",
  "2H",
  "10S",
  "AS",
  "7H",
  "9K",
  "10D",
];
var player1 = [];
var player2 = [];
var player3 = [];
var player4 = [];
function showCard() {
  for (var index = 0; index < cards.length; index++) {
    if (index == 0) {
      player1.push(cards[index]);
    } else if (index == 1) {
      player2.push(cards[index]);
    } else if (index == 2) {
      player3.push(cards[index]);
    } else if (index == 3) {
      player4.push(cards[index]);
    } else if (index == 4) {
      player1.push(cards[index]);
    } else if (index == 5) {
      player2.push(cards[index]);
    } else if (index == 6) {
      player3.push(cards[index]);
    } else if (index == 7) {
      player4.push(cards[index]);
    } else if (index == 8) {
      player1.push(cards[index]);
    } else if (index == 9) {
      player2.push(cards[index]);
    } else if (index == 10) {
      player3.push(cards[index]);
    } else if (index == 11) {
      player4.push(cards[index]);
    }
  }
  getId("#thongBao8").innerHTML = /*html*/ `
      <p>player 1: ${player1}</p>
      <p>player 2: ${player2}</p>
      <p>player 3: ${player3}</p>
      <p>player 4: ${player4}</p>
    `;
}

/********************Bài 9**********************/
function inGaCho() {
  var result = "";
  var sumChoGa = getId("#txt-m").value * 1;
  var sumFootChoGa = getId("#txt-n").value * 1;
  var soGa = sumChoGa - 1;
  var soCho = sumChoGa - soGa;
  while (soCho * 4 + soGa * 2 < sumFootChoGa) {
    soCho++;
    soGa--;
  }
  if (soCho * 4 + soGa * 2 == sumFootChoGa) {
    result = "Số gà:" + " " + soGa + "<br>" + "Số chó:" + " " + soCho;
  } else {
    result = "Input không hợp lệ!";
  }
  getId("#thongBao9").innerHTML = result;
}

/********************Bài 9**********************/
function tinhDo(h, m) {
  var ketQua = 0;
  ketQua = Math.abs(30 * h - (m / 2) * 11);
  return ketQua;
}
function inDo() {
  var soGio = getId("#txt-gio").value * 1;
  var soPhut = getId("#txt-phut").value * 1;
  var ketQua = tinhDo(soGio, soPhut);
  getId("#thongBao10").innerHTML = ketQua + " " + "độ";
}
